---
date: "2016-11-08T16:00:00+02:00"
title: "歡迎"
weight: 10
toc: false
draft: false
url: "zh-tw"
type: "home"
---

<h1 class="title is-1">Gitea - Git with a cup of tea</h1>
<h3 class="subtitle is-3">A painless self-hosted Git service.</h3>
<h4 class="subtitle">
    Gitea 是一套由社群所管理的輕量級程式碼託管解決方案，後端採用 
    [Go 語言](https://golang.org/)撰寫，採用 [MIT](https://github.com/go-gitea/gitea/blob/master/LICENSE) 授權條款。
</h4>

<div class="container">
<a class="button is-success is-large" href="https://try.gitea.io" target="_blank">試用 Gitea</a>
<a class="button is-light is-large" href="https://docs.gitea.io/zh-tw/">官方文件</a>
</div>
